package com.baleit.kwarcabsumbawa;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baleit.kwarcabsumbawa.adapters.CommentItemAdapter;
import com.baleit.wplib.ApiInterface;
import com.baleit.wplib.GetComments;
import com.baleit.wplib.models.comment.Comment;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.mikepenz.fastadapter_extensions.items.ProgressItem;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.mikepenz.itemanimators.SlideRightAlphaAnimator;

import java.util.Arrays;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class CommentActivity extends BaseActivity {

    public static final String ARG_POST = "comment_post";
    public static final String ARG_PAENT = "comment_parent";
    public static final String ARG_ALLOW_COMMENTS = "allow_comments";

    private FastAdapter fastAdapter;
    private ItemAdapter itemAdapter = new ItemAdapter();
    private ApiInterface apiInterface;
    private GetComments getComments;
    private ItemAdapter<ProgressItem> footerAdapter = new ItemAdapter<>();
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private boolean isActive;
    private boolean allowComments;
    private Integer post,parent;
    private int page=1,totalPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        if(getIntent()!=null){
            post = getIntent().getIntExtra(ARG_POST,0);
            parent = getIntent().getIntExtra(ARG_PAENT,0);
            allowComments = getIntent().getBooleanExtra(ARG_ALLOW_COMMENTS,false);
        }
        if(0==post){
            post = null;
        }
        toolbar = findViewById(R.id.commentToolbar);
        toolbarTitle = findViewById(R.id.commentPageTitle);
        progressBar = findViewById(R.id.commentsProgressBar);
        recyclerView = findViewById(R.id.commentsRecyclerView);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommentActivity.super.onBackPressed();
            }
        });
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            toolbar.setBackgroundColor(getResources().getColor(R.color.toolbarColor));
        }
        //Prepare for req
        apiInterface =  ApiClient.getClient().create(ApiInterface.class);
        getComments = new GetComments(apiInterface,getApplicationContext());
        getComments.setPost(post);
        getComments.setParent(parent);
        getComments.setListner(new GetComments.Listner() {
            @Override
            public void onSuccessful(List<Comment> commentList, int totalComments, int totalPage) {
                toolbarTitle.setText(totalComments+" "+getResources().getString(R.string.comments));
                totalPages = totalPage;
                addData(commentList);
                if(commentList.size()==0){
                    findViewById(R.id.addCommentBtn).setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    Toasty.info(getApplicationContext(),getResources().getString(R.string.no_comments),Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Adapter
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        footerAdapter = ItemAdapter.items();
        fastAdapter = FastAdapter.with(Arrays.asList(itemAdapter,footerAdapter));
        recyclerView.setAdapter(fastAdapter);
        recyclerView.setItemAnimator(new SlideRightAlphaAnimator());
        fastAdapter.withSelectable(true);
        fastAdapter.withEventHook(new CommentItemAdapter.CommentItemClickEvent());
        sendRequest(page);
    }

    private void sendRequest(int page){
        getComments.setPage(page);
        getComments.execute();
    }

    private void addData(List<Comment> commentList){
        for(Comment c:commentList){
            itemAdapter.add(new CommentItemAdapter(getApplicationContext(),c));
        }
        proceed();
    }

    private void proceed(){
        progressBar.setVisibility(View.GONE);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(int currentPage) {
                footerAdapter.clear();
                footerAdapter.add(new ProgressItem().withEnabled(false));
                if(page<totalPages) {
                    page++;
                    Toasty.info(getApplicationContext(),getString(R.string.loading_more_comments),Toast.LENGTH_SHORT).show();
                    sendRequest(page);
                }else{
                    Toasty.success(getApplicationContext(),getResources().getString(R.string.all_comments_loaded),Toast.LENGTH_SHORT).show();
                    footerAdapter.clear();
                }
            }
        });
    }

    public void addComment(View view){
        if(allowComments) {
            Intent intent = new Intent(getApplicationContext(), AddCommentActivity.class);
            intent.putExtra(AddCommentActivity.ARG_PARENT, parent);
            intent.putExtra(AddCommentActivity.ARG_POST, post);
            startActivity(intent);
        }else {
            Toasty.info(getApplicationContext(),"New comments are not allowed on this posts",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.action_add_comments).setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.action_add_comments){
            addComment(null);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive=false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive=true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
