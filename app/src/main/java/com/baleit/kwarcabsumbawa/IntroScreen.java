package com.baleit.kwarcabsumbawa;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.stephentuso.welcome.BasicPage;
import com.stephentuso.welcome.WelcomeActivity;
import com.stephentuso.welcome.WelcomeConfiguration;

public class IntroScreen extends WelcomeActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
    }

    protected void setVivasayamLanguage(){
        final SharedPreferences sharedPref = getSharedPreferences(Config.DEF_SHAREF_PREF, Context.MODE_PRIVATE);;
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setIcon(R.mipmap.ic_launcher);
        builderSingle.setTitle("Choose language");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item);
        arrayAdapter.add("English");
        arrayAdapter.add("Tamil");
        arrayAdapter.add("Malayalam");
        arrayAdapter.add("Telugu");
        arrayAdapter.add("Kanndam");
        arrayAdapter.add("Hindi");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                Toast.makeText(getApplicationContext(),strName,Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("language",which);
                editor.apply();
            }
        });
        builderSingle.show();
    }

    @Override
    protected WelcomeConfiguration configuration() {
        return new WelcomeConfiguration.Builder(this)
                .defaultTitleTypefacePath("fonts/Quicksand-Medium.ttf")
                .defaultDescriptionTypefacePath("fonts/OpenSans-Regular.ttf")
                .defaultHeaderTypefacePath("fonts/Quicksand-Medium.ttf")
                .page(new BasicPage(R.drawable.pramuka,
                        "Selamat Datang",
                        "Aplikasi berita Kwartil Cabang Sumbawa Besar")
                        .background(R.color.primary)
                )
                .page(new BasicPage(R.drawable.android_apps,
                        "Baca berita lebih mudah",
                         "Pramuka Kwartil Cabang Sumbawa hadir dengan inovasi seru. Aplikasi berbasis Android yang memudahkan pengguna dalam mencari informasi seputar Pramuka di Kota Sumbawa")
                        .background(R.color.primary)
                )
                .page(new BasicPage(R.drawable.notif,
                        "Notifikasi menarik",
                        "Melaui fitur push notification, anda dapat menerima informasi terbaru.")
                        .background(R.color.primary)
                )
                .page(new BasicPage(R.drawable.bookmark,
                        "Simpan sebagai berita favorit!",
                        "Anda dapat menyimpan berita secara offline. Sehingga dapat dibaca kapanpun.")
                        .background(R.color.primary)
                )
                .swipeToDismiss(true)
                .exitAnimation(android.R.anim.fade_out)
                .build();
    }
}
